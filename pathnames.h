/* Pathnames library
 *
 *  @file pathnames.h
 *
 *  Copyright (C) 2006 Sunrise Telephone Systems Ltd. All rights reserved.
 *  Copyright (C) 2009 Sunrise Telephone Systems KK. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */

#ifndef PATHNAMES_H
#define PATHNAMES_H


// --------------------------------------------------------------------------
// filesystem special characters
// --------------------------------------------------------------------------

#define FILESYSTEM_DIRECTORY_SEPARATOR '/'

#define FILESYSTEM_EXTENSION_SEPARATOR '.'

#define FILESYSTEM_HOMEDIR_WILDCARD '~'

#define FILESYSTEM_WORKDIR_WILDCARD '.'


// --------------------------------------------------------------------------
// maximum length of a path
// --------------------------------------------------------------------------

#define MAX_PATHNAME_LENGTH 512


// --------------------------------------------------------------------------
// function fileExistAtPath(path)
// --------------------------------------------------------------------------
//
// check if file at path exists

int fileExistsAtPath(const char *pathname);


// --------------------------------------------------------------------------
// macro fileDoesNotExistAtPath(path)
// --------------------------------------------------------------------------
//
// check if file at path does not exist

#define fileDoesNotExistAtPath(path) \
    !(fileExistsAtPath(path))


// --------------------------------------------------------------------------
// macro isPathname(path)
// --------------------------------------------------------------------------
//
// check if path contains a filesystem directory separator

#define isPathname(path) \
    (strchr((path), FILESYSTEM_DIRECTORY_SEPARATOR) != NULL)


// --------------------------------------------------------------------------
// macro isNotPathname(path)
// --------------------------------------------------------------------------
//
// check if path does not contain a filesystem directory separator

#define isNotPathname(path) \
    (strchr((path), FILESYSTEM_DIRECTORY_SEPARATOR) == NULL)


// --------------------------------------------------------------------------
// macro isAbsolutePathname(path)
// --------------------------------------------------------------------------
//
// check if path starts with a filesystem directory separator

#define isAbsolutePathname(path) \
    ((path != NULL) && (path[0] == FILESYSTEM_DIRECTORY_SEPARATOR))


// --------------------------------------------------------------------------
// macro isNotAbsolutePathname(path)
// --------------------------------------------------------------------------
//
// check if path does not start with a filesystem directory separator

#define isNotAbsolutePathname(path) \
    ((path == NULL) && (path[0] != FILESYSTEM_DIRECTORY_SEPARATOR))


// --------------------------------------------------------------------------
// function copyStrFromWorkingDirectory(str)
// --------------------------------------------------------------------------
//
// get string with current working directory

int copyStrFromWorkingDirectory(char *str);


// --------------------------------------------------------------------------
// function copyStrFromUserHomeDirectory(str)
// --------------------------------------------------------------------------
//
// get string with current user's home directory

int copyStrFromUserHomeDirectory(char *str);


// --------------------------------------------------------------------------
// function copyStrFilenameByRemovingDirectory(str, path)
// --------------------------------------------------------------------------
//
// get string with filename and extension by removing directory from path

int copyStrFilenameByRemovingDirectory(char *str, const char *pathname);


// --------------------------------------------------------------------------
// function copyStrFilenameByRemovingDirectoryAndFileExtension(str, path)
// --------------------------------------------------------------------------
//
// get string with base filename by removing directory and file extension
// from path

int copyStrFilenameByRemovingDirectoryAndFileExtension(char *str,
                                                 const char *pathname);


// --------------------------------------------------------------------------
// function modifyFilenameByAppendingFileExtension(filename, extension)
// --------------------------------------------------------------------------
//
// append file extension to filename

int modifyFilenameByAppendingFileExtension(char *filename,
                                     const char *extension);


// --------------------------------------------------------------------------
// function modifyFilenameByReplacingFileExtension(filename, extension)
// --------------------------------------------------------------------------
//
// replace file extension in filename

int modifyFilenameByReplacingFileExtension(char *filename,
                                     const char *extension);


// --------------------------------------------------------------------------
// function copyStrFileExtensionByRemovingDirectoryAndFilename(str, path)
// --------------------------------------------------------------------------
//
// get string with file extension by removing directory and filename from
// path

int copyStrFileExtensionByRemovingDirectoryAndFilename(char *str,
                                                 const char *pathname);


// --------------------------------------------------------------------------
// function copyStrDirectoryByRemovingFilenameAndExtension(str, path)
// --------------------------------------------------------------------------
//
// get string with directory by removing filename and extension from path

int copyStrDirectoryByRemovingFilenameAndExtension(char *str,
                                             const char *pathname);


// --------------------------------------------------------------------------
// function modifyPathnameByAppendingFilename(pathname, filename)
// --------------------------------------------------------------------------
//
// append file filename to pathname

int modifyPathnameByAppendingFilename(char *pathname, const char *filename);


// --------------------------------------------------------------------------
// function copyStrPathnameByExpandingTildeInPath(str, path)
// --------------------------------------------------------------------------
//
// get string with full pathname by expanding tilde in path

int copyStrPathnameByExpandingTildeInPath(char *str, const char *pathname);


// --------------------------------------------------------------------------
// function copyStrPathnameByExpandingLeadingDotInPath(str, path)
// --------------------------------------------------------------------------
//
// get string with full pathname by expanding leading dot in path

int copyStrPathnameByExpandingLeadingDotInPath(char *str,
                                         const char *pathname);


// --------------------------------------------------------------------------
// function copyStrPathnameByExpandingLeadingDotDotInPath(str, path)
// --------------------------------------------------------------------------
//
// get string with full pathname by expanding leading dotdot in path

int copyStrPathnameByExpandingLeadingDotDotInPath(char *str,
                                            const char *pathname);


// --------------------------------------------------------------------------
// function copyStrPathnameByExpandingTrailingDotInPath(str, path)
// --------------------------------------------------------------------------
//
// get string with full pathname by expanding trailing dot in path with 
// filename

int copyStrPathnameByExpandingTrailingDotInPath(char *str,
                                          const char *pathname,
                                          const char *filename);


#endif /* PATHNAMES_H */

// END OF FILE