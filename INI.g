/* INI to Property List File Conversion Utility
 *
 *  @file INI.g
 *  INI grammar
 *
 *  INI grammar in ANTLR EBNF notation
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */

grammar INI;

options {

// *** strict LL(1) ***

	backtrack = no;
	k = 1;
}

ini_file :
    COMMENT* section* EOF ;

section :
    section_header section_body ;

section_header :
    '[' IDENTIFIER ']' COMMENT? EOL ;

section_body :
    ( ( definition | COMMENT ) EOL )* ;

definition :
    IDENTIFIER
    ( ASSIGN_OVERWRITE | ASSIGN_ACCUMULATE {}) value_or_value_list COMMENT? ;

value_or_value_list :
    value ( ',' value )* ;

value :
    ( NUMBER | QUOTED_STRING | UNQUOTED_STRING {}) ;

IDENTIFIER :
    ( UNDERSCORE | LETTER {}) ( UNDERSCORE | LETTER | DIGIT {})* ;

fragment
UNDERSCORE :
    '_' ;

fragment
LETTER :
    'A' .. 'Z' | 'a' .. 'z' {};

fragment
DIGIT :
    '0' .. '9' ;

COMMENT :
    ';' ANY_CHAR* ;

ASSIGN_OVERWRITE :
    '=' ;

ASSIGN_ACCUMULATE :
    '=>' ;

NUMBER :
    DIGIT+ ( '.' DIGIT+ )? ;

QUOTED_STRING :
    '"' ANY_CHAR_EXCL_DOUBLE_QUOTE+ '"' |
    '\'' ANY_CHAR_EXCL_SINGLE_QUOTE+ '\'' ;

UNQUOTED_STRING :
    ANY_CHAR_EXCL_QUOTES_COMMA_SEMI+ ;

COMMA :
    ',' ;

EOL :
    ( ASCII_CR ASCII_LF? ) | (  ASCII_LF ASCII_CR? ) ;

fragment
ASCII_CR :
    '\r' ;

fragment
ASCII_LF :
    '\n' ;

fragment
ASCII_CTRL_CHAR :
    // any ASCII control charater
    '\u0000' .. '\u001f' | '\u007f';

fragment
ANY_CHAR :
    // any printable character
    ~( ASCII_CTRL_CHAR )	;

fragment
ANY_CHAR_EXCL_SINGLE_QUOTE :
    // any printable character excluding single quote
    ~( ASCII_CTRL_CHAR | '\'' )	;

fragment
ANY_CHAR_EXCL_DOUBLE_QUOTE :
    // any printable character excluding double quote
    ~( ASCII_CTRL_CHAR | '"' )	;

fragment
ANY_CHAR_EXCL_QUOTES_COMMA_SEMI :
    // any printable character excluding quotes, comma and semicolon
    ~( ASCII_CTRL_CHAR | '\'' | '"' | ',' | ';' ) ;

WHITESPACE :
    ASCII_SP | ASCII_TAB
    {} // make ANTLRworks display separate branches
    { $channel = HIDDEN; } // ignore
    ;

fragment
ASCII_SP :
    ' ' ;

fragment
ASCII_TAB :
    '\t' ;

EOF :
    '\u007f'; // system dependent

// END OF FILE
