/* INI to Property List File Conversion Utility
 *
 *  @file i2p_parser.c
 *  parser implementation
 *
 *  Syntax analysis for INI files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */


#include <stdio.h>
#include <stdint.h>

#include "i2p_tokens.h"
#include "i2p_parser.h"


// --------------------------------------------------------------------------
// Tokenset type
// --------------------------------------------------------------------------

typedef uint_fast32_t i2p_tokenset_t;

#if (I2P_NUMBER_OF_TOKENS > 32)
#error "Fatal error: number of tokens must not exceed 32."
#endif


// --------------------------------------------------------------------------
// macro:  SET( token )
// --------------------------------------------------------------------------
//
// Returns a set with member <token>

#define SET(_token)   (1 << (_token))


// --------------------------------------------------------------------------
// macro:  IN_SET( token, set )
// --------------------------------------------------------------------------
//
// Returns true if <token> is member of tokenset <set>,  otherwise false.

#define IN_SET(_token, _set)   (((1 << (_token)) & (_set)) != 0)


// --------------------------------------------------------------------------
// macro:  UNION( set1, set2 )
// --------------------------------------------------------------------------
//
// Returns the union of tokensets <set1> and <set2>.

#define UNION(_set1, _set2)   ((_set1) | (_set2))


// --------------------------------------------------------------------------
// FIRST sets
// --------------------------------------------------------------------------

#define FIRST_ini_file ( \
    1 << TOKEN_COMMENT + \
    1 << TOKEN_OPENING_BRACKET + \
    1 << TOKEN_END_OF_FILE )

#define FIRST_section ( \
    1 << TOKEN_OPENING_BRACKET )

#define FIRST_section_header ( \
    1 << TOKEN_OPENING_BRACKET )

#define FIRST_section_body ( \
    1 << TOKEN_IDENTIFIER + \
    1 << TOKEN_COMMENT )

#define FIRST_definition ( \
    1 << TOKEN_IDENTIFIER )

#define FIRST_value_list ( \
    1 << TOKEN_NUMBER + \
    1 << TOKEN_QUOTED_STRING + \
    1 << TOKEN_UNQUOTED_STRING )

#define FIRST_value ( \
    1 << TOKEN_NUMBER + \
    1 << TOKEN_QUOTED_STRING + \
    1 << TOKEN_UNQUOTED_STRING )


// --------------------------------------------------------------------------
// FOLLOW sets
// --------------------------------------------------------------------------

#define FOLLOW_ini_file ( \
    1 << TOKEN_END_OF_FILE )

#define FOLLOW_section ( \
    1 << TOKEN_OPENING_BRACKET + \
    1 << TOKEN_END_OF_FILE )

#define FOLLOW_section_header ( \
    1 << TOKEN_IDENTIFIER + \
    1 << TOKEN_COMMENT )

#define FOLLOW_section_body ( \
    1 << TOKEN_OPENING_BRACKET + \
    1 << TOKEN_END_OF_LINE )

#define FOLLOW_definition ( \
    1 << TOKEN_END_OF_LINE )

#define FOLLOW_value_list ( \
    1 << TOKEN_COMMENT + \
    1 << TOKEN_END_OF_LINE )

#define FOLLOW_value ( \
    1 << TOKEN_COMMA + \
    1 << TOKEN_COMMENT + \
    1 << TOKEN_END_OF_LINE )


// --------------------------------------------------------------------------
// Symbol key type
// --------------------------------------------------------------------------

typedef unsigned int i2p_symbol_key_t;


// --------------------------------------------------------------------------
// Symbol table entry type
// --------------------------------------------------------------------------

typedef struct /* i2p_symbol_table_entry_s */ {
    i2p_symbol_type_t type;
     i2p_symbol_key_t key;
    
} i2p_symbol_table_entry_s;


// --------------------------------------------------------------------------
// Symbol type
// --------------------------------------------------------------------------

typedef struct /* i2p_symbol_s */ {
           i2p_token_t token;
      i2p_symbol_key_t key;
    i2p_lexer_status_t status;
} i2p_symbol_s;


// --------------------------------------------------------------------------
// Parser state type
// --------------------------------------------------------------------------

typedef struct /* i2p_parser_s */ {
     i2p_lexer_t lexer;
    i2p_symbol_s lookahead_sym;
    i2p_symbol_s current_sym;
    unsigned int error_count;
} i2p_parser_s;


// --------------------------------------------------------------------------
// private function:  getsym( parser_state )
// --------------------------------------------------------------------------
//
// Reads a  new symbol  from the input stream,  stores the previous lookahead
// symbol as the  current symbol,  stores the new symbol as lookahead symbol,
// then returns the current symbol's token.

static inline i2p_token_t getsym(i2p_parser_s *p) {
    p->current_sym = p->lookahead_sym;
    p->lookahead_sym.token = i2p_lexer_getsym(p->lexer,
                                             &p->lookahead_sym.key,
                                             &p->lookahead_sym.status);
    return p->current_sym.token;
} // end getsym


// --------------------------------------------------------------------------
// private function:  lookahead( parser_state )
// --------------------------------------------------------------------------
//
// Returns the token of the current lookahead symbol.  This function does not
// read from the input stream.  Subsequent calls to this function will return
// the  same token  again.  To read  further symbols  from the  input stream,
// function getsym must be used.

static inline i2p_token_t lookahead(i2p_parser_s *p) {
    return p->lookahead_sym.token;
} // end lookahead


// --------------------------------------------------------------------------
// private function:  i2p_parse( parser_state )
// --------------------------------------------------------------------------

static void i2p_parse(i2p_parser_s *p) {
    i2p_token_t token;
    
    // start symbol: ini_file
    if (IN_SET(token, FOLLOW_ini_file))
        token = i2p_ini_file(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FOLLOW_ini_file);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_ini_file);
    } // end if
    
    return;
} // end i2p_parse


// --------------------------------------------------------------------------
// private function:  i2p_ini_file( parser_state )
// --------------------------------------------------------------------------
//
// Implements production ini_file:
//
// ini_file =
//     COMMENT* section* EOF ;

static i2p_token_t i2p_ini_file(i2p_parser_s *p) {
    i2p_token_t token;

    token = lookahead(p);
    
    // COMMENT*
    while (IN_SET(token, FIRST_comment))
        token = i2p_comment(lp);
    
    // section*
    while (IN_SET(token, FIRST_section))
        token = i2p_section(p);
    
    // EOF
    if (token == TOKEN_END_OF_FILE)
        getsym(p); // consume EOF
    else {
        p->error_count++;
        // report error
        mismatch(p, token, SET(TOKEN_END_OF_FILE));
        // skip to end of file
        token = skip_to(p, SET(TOKEN_END_OF_FILE));
    } // end if
    
    return token;
} // end i2p_ini_file


// --------------------------------------------------------------------------
// private function:  i2p_section( parser_state )
// --------------------------------------------------------------------------
//
// Implements production section:
//
// section =
//     section_header section_body ;

static i2p_token i2p_section(i2p_parser_s *p) {
    i2p_token_t token;
    
    token = lookahead(p);
    
    // section_header
    if (IN_SET(token, FIRST_section_header))
        token = i2p_section_header(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_section_header);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_section_header);
    } // end if
    
    // section_body
    if (IN_SET(token, FIRST_section_body))
        token = i2p_section_body(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_section_body);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_section_body);
    } // end if
    
    return token;
} // end i2p_section


// --------------------------------------------------------------------------
// private function:  i2p_section_header( parser_state )
// --------------------------------------------------------------------------
//
// Implements production section_header:
//
// section_header =
//     "[" IDENTIFIER "]" COMMENT? EOL ;

static i2p_token i2p_section_header(i2p_parser_s *p) {
    i2p_token_t token;
    
    getsym(p); // consume "["
    token = lookahead(p);
    
    // IDENTIFIER
    if (token == TOKEN_IDENTIFIER) {
        token = getsym(p);
        // TO DO : create new dictionary with identifier as key
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, SET(TOKEN_IDENTIFIER));
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_section_header);
        // bail out
        return token;
    } // end if
    
    // "]"
    if (token == TOKEN_CLOSING_BRACKET)
        token = getsym(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, SET(TOKEN_CLOSING_BRACKET));
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_section_header);
        // bail out
        return token;
    } // end if
    
    // COMMENT?
    if (token == TOKEN_COMMENT) {
        getsym(p); // consume COMMENT
        token = lookahead(p);
        // TO DO : insert comment into current dictionary
    } // end if
    
    if (token == TOKEN_END_OF_LINE)
        token = getsym(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, SET(TOKEN_END_OF_LINE));
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_section_header);
    } // end if
    
    return token;
} // end i2p_section_header


// --------------------------------------------------------------------------
// private function:  i2p_section_body( parser_state )
// --------------------------------------------------------------------------
//
// Implements production section_body:
//
// section_body =
//     ( ( definition | COMMENT ) EOL )* ;

static i2p_token i2p_section_body(i2p_parser_s *p) {
    i2p_token_t token;
    
    token = lookahead(p);
    
    // ( ( definition | COMMENT ) EOL )*
    while (IN_SET(token, FIRST_section_body)) {
        switch (token) {
            case IDENTIFIER :
                token = i2p_definition(p);
                break;
            case COMMENT :
                token = i2p_comment(p);
                break;
            default :
                abort();
        } // end switch
        
        if (token == TOKEN_END_OF_LINE) {
            getsym(p); // consume EOL
            token = lookahead(p);
        }
        else {
            p->error_count++;
            // report error
            mismatch(p, token, SET(TOKEN_END_OF_LINE));
            // skip to recovery symbol
            token = skip_to(p, FOLLOW_definition);
        } // end if
    } // end while
        
    return token;    
} // end i2p_section_body


// --------------------------------------------------------------------------
// private function:  i2p_definition( parser_state )
// --------------------------------------------------------------------------
//
// Implements production definition:
//
// definition =
//     IDENTIFIER
//     ( ASSIGN_OVERWRITE | ASSIGN_ACCUMULATE ) value_list COMMENT? ;

static i2p_token i2p_definition(i2p_parser_s *p) {
    i2p_token_t token;
    
    getsym(p); // consume IDENTIFIER
    token = lookahead(p);

    // TO DO : create new key/value entry in current dictionary
    //         with identifier as key
    
    // ( ASSIGN_OVERWRITE | ASSIGN_ACCUMULATE )
    if (token == ASSIGN_OVERWRITE) {
        getsym(p); // consume "="
        token = lookahead(p);
    }
    else if (token == ASSIGN_ACCUMULATE) {
        getsym(p); // consume "=>"
        token = lookahead(p);
    }
    else /* invalid */ {
        abort();
    } // end if
    
    // value_list
    if (IN_SET(token, FIRST_value_list)) {
        token = i2p_value_list(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_value_list);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_value_list);
    } // end if
    
    // COMMENT?
    if (token == TOKEN_COMMENT) {
        getsym(p); // consume COMMENT
        token = lookahead(p);
        // TO DO : insert comment into current dictionary
    } // end if
    
    return token;
} // end i2p_definition


// --------------------------------------------------------------------------
// private function:  i2p_value_list( parser_state )
// --------------------------------------------------------------------------
//
// Implements production value_list:
//
// value_list =
//     value ( ',' value )* ;

static i2p_token i2p_value_list(i2p_parser_s *p) {
    i2p_token_t token;

    token = lookahead(p);
    
    // value
    if (IN_SET(token, FIRST_value))
        token = i2p_value(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_value);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_value);
    } // end if
    
    // ( ',' value )*
    while (token == TOKEN_COMMA) {
        
        getsym(p); // consume COMMA
        token = lookahead(p);
        
        if (IN_SET(token, FIRST_value))
            token = i2p_value(p);
        else {
            p->error_count++;
            // report error
            mismatch(p, token, FIRST_value);
            // skip to recovery symbol
            token = skip_to(p, FOLLOW_value);
        } // end if
        
    } // end while
    
    return token;
} // end i2p_value_list


// --------------------------------------------------------------------------
// private function:  i2p_value( parser_state )
// --------------------------------------------------------------------------
//
// Implements production value:
//
// value =
//     ( NUMBER | QUOTED_STRING | UNQUOTED_STRING )

static i2p_token i2p_value(i2p_parser_s *p) {
    i2p_token_t token;
    
    token = lookahead(p);
    
    // ( NUMBER | QUOTED_STRING | UNQUOTED_STRING )
    switch (token) {
        case TOKEN_NUMBER :
            getsym(p); // consume
            token = lookahead(p);
            // TO DO : add value to value list
            break;
        case TOKEN_QUOTED_STRING :
            getsym(p); // consume
            token = lookahead(p);
            // TO DO : add value to value list
            break;
        case TOKEN_UNQUOTED_STRING :
            getsym(p); // consume
            token = lookahead(p);
            // TO DO : add value to value list
            break;
        default : // invalid
            abort();
    } // end switch
    
    return token;
} // end i2p_value


// --------------------------------------------------------------------------
// private function:  mismatch( p, found_token, expected_set )
// --------------------------------------------------------------------------
//
// Prints an error message to stderr.

static void mismatch(i2p_parser_s *p,
                      i2p_token_t found_token,
                   i2p_tokenset_t expected_set) {
    
    unsigned int row = 0, col = 0, index, token_count;
    i2p_token_t token_list[I2P_NUMBER_OF_TOKENS];
    i2p_token_t token;
    
    if (expected_set == 0) return;
    
    // create token list from expected_set
    index = 0; token = 0;
    while (token < I2P_NUMBER_OF_TOKENS) {
        if (IN_SET(token, expected_set)) {
            token_list[index] = token;
            index++;
        } // end if
        token++;
    } // end while
    
    if (index == 0) return;
    
    // remember number of tokens in list
    token_count = index;
    
    // get position of offending symbol
    i2p_lexer_getpos(p->lexer, &row, &col, NULL);

    // print name of offending symbol and its position
    fprintf(stderr, "syntax error in line:%i, col:%i, found %s, expected ",
           row, col, i2p_token_name(found_token));
    
    // print list of names of expected tokens
    index = 0;
    while (index < token_count) {
        token = token_list[index];
        fprintf(stderr, "%s", i2p_token_name(token));
        if (index + 2 < token_count)
            fprintf(stderr, ", ");
        else if (index + 2 == token_count)
            fprintf(stderr, " or ");
        else
            fprintf(stderr, "\n");
        index++;
    } // end while
    
    return;
} // end mismatch


// --------------------------------------------------------------------------
// private function:  skip_to( parser_state, recovery_set )
// --------------------------------------------------------------------------
//
// Skips tokens until lookahead token is a member of set <recovery_set>.

static i2p_token_t skip_to(i2p_parser_s *p,
                         i2p_tokenset_t recovery_set) {
    i2p_token_t token;
    
    token = lookahead(p);
    
    while NOT (IN_SET(token, recovery_set) {
        getsym(p); // skip
        token = lookahead(p);
    } // end while
    
    return token;
} // end skip_to


// END OF FILE
