/* INI to Property List File Conversion Utility
 *
 *  @file i2p_lexer.c
 *  lexer implementation
 *
 *  Lexical analysis for INI files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */


// ---------------------------------------------------------------------------
// Standard library imports
// ---------------------------------------------------------------------------

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

// ---------------------------------------------------------------------------
// Project imports
// ---------------------------------------------------------------------------

#include "common.h"
#include "hash.h"
#include "ASCII.h"
#include "KVS.h"
#include "i2p_build_params.h"
#include "i2p_lexer.h"


// ---------------------------------------------------------------------------
// Lexeme buffer size
// ---------------------------------------------------------------------------

#define I2P_MAX_LEXEME_LENGTH 256


// ---------------------------------------------------------------------------
// Lexeme buffer type
// ---------------------------------------------------------------------------

typedef struct /* lexbuf_t */ {
    cardinal length;
    char string[I2P_MAX_LEXEME_LENGTH];
} lexbuf_t;


// ---------------------------------------------------------------------------
// Lexer state type
// ---------------------------------------------------------------------------

typedef /* i2p_lexer_s */ struct {
    
    // configuration parameters
    FILE *sourcefile;                   // source file
    kvs_table_t lextab;                 // lexeme table
    bool skip_comments;                 // ignore comments if true
    bool enable_value_lists;            // comma separated values if true
    
    // return values
    i2p_token_t token;                  // token to be returned
    cardinal lexkey;                    // lexeme key to be returned
    i2p_lexer_status_t status;          // status to be returned
    
    // counters
    file_pos_t token_pos;               // position of current symbol
    file_pos_t current_pos;             // position of current character
    
    // flags
    bool seen_assign;                   // right hand side of assign token
    bool end_of_file;                   // end-of-file has been reached
    
    // lexeme buffer
    lexbuf_t lexeme;
    
    // offending character
    char offending_char;
    file_pos_t offending_char_pos;
} i2p_lexer_s;

#define NOT_EOF(_lexer) (_lexer->end_of_file == false)
#define EOF_REACHED(_lexer) (_lexer->end_of_file == true)


// ==========================================================================
// P R I V A T E   F U N C T I O N   P R O T O T Y P E S
// ==========================================================================

static fmacro uchar_t _readchar(i2p_lexer_s *lexer);

static fmacro uchar_t _nextchar(i2p_lexer_s *lexer);

static fmacro uchar_t get_ident(i2p_lexer_s *lexer);

static fmacro uchar_t get_quoted_string(i2p_lexer_s *lexer);

static fmacro uchar_t get_unquoted_value(i2p_lexer_s *lexer);

static fmacro uchar_t get_comment(i2p_lexer_s *lexer);

static fmacro uchar_t get_escaped_char(i2p_lexer_s *lexer);

static fmacro void add_lexeme_to_lextab(i2p_lexer_s *lexer);

static fmacro uchar_t skip_comment(i2p_lexer_s *lexer);

static fmacro uchar_t skip_past_end_of_line(i2p_lexer_s *lexer);


// ==========================================================================
// P U B L I C   F U N C T I O N   I M P L E M E N T A T I O N S
// ==========================================================================

#define readchar(v) _readchar(this_lexer) /* v = void */
#define nextchar(v) _nextchar(this_lexer) /* v = void */

// --------------------------------------------------------------------------
// function:  i2p_new_lexer(infile, lextab, status)
// --------------------------------------------------------------------------
//
// Creates  and  returns  a  new  lexer object  associated  with  source file 
// <infile> and lexeme table <lextab>.  The status of the operation is passed
// back in <status> unless NULL is passed in for <status>.
//
// Returns NULL if the lexer object could not be created.

i2p_lexer_t i2p_new_lexer(FILE *infile,
                   kvs_table_t lextab,
            i2p_lexer_status_t *status) {
    
    i2p_lexer_s *new_lexer;    
    
    // assert pre-conditions
    
    if (infile == NULL) {
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    errno = 0;
    rewind(infile);
    if (errno != 0) {
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    if (lextab == NULL) {
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    // allocate a new lexer object
    new_lexer = (i2p_lexer_s) malloc(sizeof(i2p_lexer_s));
    
    if (new_lexer == NULL) {
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_ALLOCATION_FAILED);
        return NULL;
    } // end if
    
    // initialise the new lexer object
    
    // configuration parameters
    new_lexer->sourcefile = infile;
    new_lexer->lextab = lextab;
    new_lexer->skip_comments = false;
    new_lexer->enable_value_lists = false;
    
    // return values
    new_lexer->token = 0;
    new_lexer->lexkey = 0;
    
    // counters
    SET_FPOS(new_lexer->token_pos, 1, 1);
    SET_FPOS(new_lexer->current_pos, 1, 1);
    
    // lexer flags
    new_lexer->seen_assign = false;
    new_lexer->end_of_file = false;
    
    // input buffer
    new_lexer->lexeme.length = 0;
    new_lexer->lexeme.string[0] = CSTRING_TERMINATOR;
    
    // offending character
    new_lexer->offending_char = 0;
    SET_FPOS(new_lexer->offending_char_pos, 0, 0);
    
    // return the initialised lexer object
    ASSIGN_BY_REF(status, I2P_LEXER_STATUS_SUCCESS);
    return (i2p_lexer_t) new_lexer;
} // end i2p_new_lexer;


// ---------------------------------------------------------------------------
// function:  i2p_lexer_set_options(lexer, skip_comments, enable_value_lists)
// ---------------------------------------------------------------------------
//
// Changes the options of <lexer> to <options>.  The function returns  without
// any action if NULL is passed in for <lexer>.

void i2p_lexer_set_options(i2p_lexer_t lexer,
                           bool skip_comments,
                           bool enable_value_lists) {
    
    i2p_lexer_s *this_lexer = (i2p_lexer_s *) lexer;
    
    if (lexer != NULL) {
        this_lexer->skip_comments = (skip_comments == true);
        this_lexer->enable_value_lists = (enable_value_lists == true);
    } // end if
    
    return;
} // end if


// ---------------------------------------------------------------------------
// function:  i2p_lexer_getsym(lexer, lexeme, status)
// ---------------------------------------------------------------------------
//
// Reads one symbol from the input stream of lexer <lexer>, returns its token,
// and passes a key for its lexeme back in <lexeme> unless  NULL  is passed in
// for <lexeme>.  The  status  of  the  operation  is  passed back in <status>
// unless NULL is passed in for <status>.

i2p_token_t i2p_lexer_getsym(i2p_lexer_t lexer,
                                cardinal *lexeme
                      i2p_lexer_status_t *status) {
    
    register i2p_lexer_s *this_lexer = (i2p_lexer_s *) lexer;
    register uchar_t ch;
    bool ignore_token;
    
    // assert pre-condition
    if (lexer == NULL) {
        ASSIGN_BY_REF(lexeme, 0);
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return TOKEN_ILLEGAL_CHARACTER;
    } // end if
    
    // clear lexeme
    this_lexer->lexkey = 0;
    this_lexer->lexeme.length = 0;
    this_lexer->lexeme.string[0] = CSTRING_TERMINATOR;
    
    ch = nextchar();
    repeat {
        // reset loop exit condition
        ignore_token = false;

        // skip all whitespace and tab characters
        while ((NOT_EOF(this_lexer)) &&
               ((ch == WHITESPACE) || (ch == TAB))) {
            // skip the current character
            readchar();
            // take a peek at the next one
            ch = nextchar();
        } // end while;
        
        // remember position at the start of the symbol
        this_lexer->token_pos = this_lexer->current_pos;
        
        // start optimistically
        this_lexer->status = I2P_LEXER_STATUS_SUCCESS;
                
        // check for end-of-file
        if (EOF_REACHED(this_lexer)) {
            this_lexer->token = TOKEN_END_OF_FILE;
        } // end eof check
        
        // check for end-of-line
        if (ch == EOL) {
            this_lexer->seen_assign = false;
            this_lexer->token = TOKEN_END_OF_LINE;
        } // end eol check
        
        // check for comment
        else if (ch == SEMICOLON) {
            if (this_lexer->skip_comments == true) {
                ch = skip_comment(this_lexer);
                ignore_token = true;
            }
            else /* return comment as token */ {
                ch = get_comment(this_lexer);
                if (this_lexer->status == I2P_LEXER_STATUS_SUCCESS)
                    add_lexeme_to_lextab(this_lexer);
            } // end if
        } // end comment check
        
        else if /* left hand side */ (this_lexer->seen_assign == false) {
            
            // any of identifer, "=", "=>", "[" and "]" are legal
            
            // check for identifier
            if (ch == UNDERSCORE || IS_LETTER(ch)) {
                ch = get_ident(this_lexer);
                if (this_lexer->status == I2P_LEXER_STATUS_SUCCESS)
                    add_lexeme_to_lextab(this_lexer);                
            }
            
            // check for "=" and "=>"
            else if (ch == EQUAL_SIGN) {
                ch = readchar();
                ch = nextchar();
                if (ch != GREATER_THAN) {
                    /* found "=" */
                    ch = readchar();
                    ch = nextchar();
                    this_lexer->token = TOKEN_ASSIGN_OVERWRITE;
                }
                else /* found "=>" */ {
                    this_lexer->token = TOKEN_ASSIGN_ACCUMULATE;
                } // end if
                this_lexer->seen_assign = true;
            }
            
            // check for "["
            else if (ch == OPENING_BRACKET) {
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_OPENING_BRACKET;
            }
            
            // check for "]"
            else if (ch == CLOSING_BRACKET) {
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_CLOSING_BRACKET;
            }
            
            // illegal character
            else /* any other char */ {
                this_lexer->offending_char = readchar();
                this_lexer->offending_char_pos = this_lexer->current_pos;
                ch = nextchar();
                this_lexer->token = TOKEN_ILLEGAL_CHAR;
            } // end if
        }
                            
        else /* right hand side */ {
            
            // check for quoted string
            if ((ch == SINGLE_QUOTE) || (ch == DOUBLE_QUOTE)) {
                ch = get_quoted_string(this_lexer);
                if (this_lexer->status == I2P_LEXER_STATUS_SUCCESS)
                    add_lexeme_to_lextab(this_lexer);
            }
                        
            else /* comma, number literal or unquoted value */ {
                
                // if value lists are enabled treat comma as a symbol
                if ((ch == COMMA) &&
                    (this_lexer->enable_value_lists == true)) {
                    readchar(); // consume COMMA
                    ch = nextchar();
                    this_lexer->token == TOKEN_COMMA;
                }
                else /* value lists are disabled */ {
                    ch = get_unquoted_value(this_lexer);
                    if (this_lexer->status == I2P_LEXER_STATUS_SUCCESS)
                        add_lexeme_to_lextab(this_lexer);
                }
            } // end if
            
        } // end if

    } until (ignore_token == false);
    
    // pass back lexeme key and status
    ASSIGN_BY_REF(lexeme, this_lexer->lexkey);
    ASSIGN_BY_REF(status, this_lexer->status);
    
    // return the token
    return this_lexer->token;
} // end i2p_lexer_getsym


// ---------------------------------------------------------------------------
// function:  i2p_lexer_getpos(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Obtains the position of the last symbol read from the input stream.  Passes
// the row back in <row>  unless NULL is passed in for <row>,  and the coloumn
// back in <col>  unless  NULL  is  passed  in  for <col>.  The status  of the
// operation is passed back in <status> unless NULL is passed in for <status>.

void i2p_lexer_getpos(i2p_lexer_t lexer,
                        cardinal *row,
                        cardinal *col,
              i2p_lexer_status_t *status) {
    
    i2p_lexer_s *this_lexer = (i2p_lexer_s *) lexer;
    
    if (lexer == NULL)
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
    else {
        ASSIGN_BY_REF(row, this_lexer->token_pos.line);
        ASSIGN_BY_REF(col, this_lexer->token_pos.col);
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_SUCCESS);
    } // end if
    
    return;
} // end i2p_lexer_getpos


// ---------------------------------------------------------------------------
// function:  i2p_offending_char(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Returns the offending character of the last read operation  and  passes its
// position  back  in  <row>  and <col>.  If no error occurred during the last
// read operation then ASCII NUL is returned  and zero is passed pack in <row>
// and <col>.  This function should only be called  after a preceeding call to
// function i2p_lexer_getsym()  returned  an error  indicating that an illegal
// or unexcpected character was found.  The status of the operation  is passed
// back in <status> unless NULL is passed in for <status>.

char i2p_offending_char(i2p_lexer_t lexer,
                           cardinal *row,
                           cardinal *col,
                 i2p_lexer_status_t *status) {
    
    i2p_lexer_s *this_lexer = (i2p_lexer_s *) lexer;
    
    if (lexer == NULL) {
        ASSIGN_BY_REF(row, 0);
        ASSIGN_BY_REF(col, 0);
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return ASCII_NUL;
    }
    else if (this_lexer->status != I2P_LEXER_STATUS_ILLEGAL_CHARACTER) {
        ASSIGN_BY_REF(row, 0);
        ASSIGN_BY_REF(col, 0);
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return ASCII_NUL;
    }
    else {
        ASSIGN_BY_REF(row, this_lexer->current_pos.line);
        ASSIGN_BY_REF(col, this_lexer->current_pos.col);
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_SUCCESS);
        return this_lexer->offending_char;
    } // end if
} // end i2p_offending_char


// ---------------------------------------------------------------------------
// function:  i2p_dispose_lexer(lexer)
// ---------------------------------------------------------------------------
//
// Disposes of lexer object <lexer>  and  closes its sourcefile if it is open. 
// The  symbol table  used  by  the lexer is  NOT  disposed of.  The status of
// the operation is passed back in <status>.

void i2p_dispose_lexer(i2p_lexer_t lexer, i2p_lexer_status_t *status) {
    
    i2p_lexer_s *this_lexer = (i2p_lexer_s *) lexer;
    
    if (lexer == NULL) {
        ASSIGN_BY_REF(status, I2P_LEXER_STATUS_INVALID_REFERENCE);
        return;
    } // end if
    
    fclose(this_lexer->sourcefile);
    free(this_lexer);
    
    ASSIGN_BY_REF(status, I2P_LEXER_STATUS_SUCCESS);
    return;
} // end i2p_dispose_lexer;

#undef readchar
#undef nextchar


// ==========================================================================
// P R I V A T E   F U N C T I O N   I M P L E M E N T A T I O N S
// ==========================================================================

// ---------------------------------------------------------------------------
// macros for private functions
// ---------------------------------------------------------------------------

#define readchar(v) _readchar(lexer) /* v = void */
#define nextchar(v) _nextchar(lexer) /* v = void */


// ---------------------------------------------------------------------------
// private function:  _readchar(lexer)
// ---------------------------------------------------------------------------
//
// Reads one character from the input stream of <lexer>  and  returns it.  The
// lexer's coloumn counter is incremented.  Returns linefeed (ASCII LF) if any
// of linefeed (ASCII LF)  or carriage return (ASCII CR)  or  a combination of
// CR and LF (CRLF or LFCR) is read.  If LF is returned,  the  lexer's coloumn
// counter is reset and its line counter is incremented.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//
// post-conditions:
//  o  new current character is the character read (consumed)
//  o  new lookahead character is the character following the character read
//  o  position counters are updated accordingly
//
// return-value:
//  o  read (consumed) character is returned

static fmacro uchar_t _readchar(i2p_lexer_s *lexer) {
    register int c;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // read one character from source file
    c = getc(lexer->sourcefile);
    
    // handle LF style end-of-line
    if (c == ASCII_LF) {
        lexer->current_pos.col = 1;
        lexer->current_pos.line++;
    }
    // handle CRLF and CR style end-of-line
    else if (c == ASCII_CR) {
        lexer->current_pos.col = 1;
        lexer->current_pos.line++;
        c = getc(lexer->sourcefile);
        if (c != NEWLINE) {
            ungetc(c, lexer->sourcefile);
        } // end if
        c = NEWLINE;
    }
    // handle end-of-file
    else if (c == EOF) {
        // set end-of-file flag if end-of-file reached
        lexer->end_of_file = (feof(lexer->sourcefile) == true);
        c = 0;
    }
    else /* any other characters */ {
        // increment row counter
        lexer->current_pos.col++;
    } // end if
    
    if (((uchar_t) c == 255) || (c == 0)) {
        printf("");
    } // end if
    
    // return character
    return (uchar_t) c;
} // end _readchar


// ---------------------------------------------------------------------------
// private function:  _nextchar(lexer)
// ---------------------------------------------------------------------------
//
// Returns the lookahead character in the input stream of <lexer>  and returns
// it without incrementing the file pointer  and  without changing the lexer's
// coloumn and line counters.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//
// post-conditions:
//  o  position counters remain unchanged
//
// return-value:
//  o  lookahead character is returned

static fmacro uchar_t _nextchar(i2p_lexer_s *lexer) {
    register int status;
    register int c;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    c = getc(lexer->sourcefile);
    
    status = ungetc(c, lexer->sourcefile);
    if (status != EOF) {
        lexer->end_of_file = false;
    }
    else {
        lexer->end_of_file = true;
        c = 0;
    } // end if
    
    return (uchar_t) c;
} // end _nextchar


// ---------------------------------------------------------------------------
// private function:  get_ident(lexer)
// ---------------------------------------------------------------------------
//
// Reads  an  identifier  from  the  input stream of <lexer>  and  returns the
// character following the identifier.
//
// This function accepts input conforming to the following syntax:
//
//  identifier := ( "_" | letter ) ( "_" | letter | digit )*
//  letter := "a" .. "z" | "A" .. "Z"
//  digit := "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//  o  the current lookahead character is the first character at the beginning
//     of the identifier.
//  o  the length of the identifier does not exceed I2P_MAX_IDENT_LENGTH.
//
// post-conditions:
//  o  lexer->lexeme.string contains the identifier,
//     followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_IDENTIFIER
//  o  lexer->lexkey contains the key for the lexeme table.
//  o  lexer->status contains I2P_LEXER_STATUS_SUCCESS.
//  o  the new lookahead character is the character following the identifier.
//
// error-conditions:
//  if the identifier exceeds I2P_MAX_IDENT_LENGTH
//  o  lexer->lexeme.string contains the significant characters only,
//     followed by a C string terminator (ASCII NUL).
//  o  otherwise, post-conditions apply.

static fmacro uchar_t get_ident(i2p_lexer_s *lexer) {
    uchar_t final_ch = 0;
    uchar_t ch;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    lexer->lexeme.length = 0;
    lexer->lexkey = HASH_INITIAL;
    
    ch = readchar();
    lexer->lexeme.string[lexer->lexeme.length] = ch;
    lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
    lexer->lexeme.length++;
    ch = nextchar();
    
    while (((IS_ALPHANUM(ch)) || (ch == UNDERSCORE)) &&
           ((lexer->lexeme.length < I2P_MAX_IDENT_LENGTH) &&
            (NOT_EOF(lexer)))) {
        ch = readchar();
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        ch = nextchar();
    } // while
    
    // any further identifier characters are not significant, skip them
    while (((IS_ALPHANUM(ch)) || (ch == UNDERSCORE)) && (NOT_EOF(lexer))) {
        ch = readchar();
        ch = nextchar();
    } // end while
        
    // terminate the lexeme
    lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
    lexer->lexkey = HASH_FINAL(lexer->lexkey);
    
    // set the token
    lexer->token = TOKEN_IDENTIFIER;
    
    // return the lookahead character
    return ch;
} // end get_ident


// ---------------------------------------------------------------------------
// private function:  get_quoted_string(lexer)
// ---------------------------------------------------------------------------
//
// Reads a  quoted string  from the input stream  of <lexer>  and  returns the
// character following the string literal.
//
// pre-conditions:
//  o  lexer is an initialised lexer object.
//  o  the current lookahead character is the delimiting quotation mark at the
//     beginning of the string literal.
//  o  the string is properly delimited with matching opening and closing
//     quotation marks, does not exceed the maximum string length and does
//     not contain any control characters.
//
// post-conditions:
//  o  lexer->lexeme.string contains the literal including delimiters,
//     followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_QUOTED_STRING.
//  o  lexer->lexkey contains the key for the lexeme table.
//  o  lexer->status contains I2P_LEXER_STATUS_SUCCESS.
//  o  the new lookahead character is the character following the literal.
//
// error-conditions:
//  o  lexer->lexeme.string contains the part of the literal before the
//     offending character, followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_ILLEGAL_CHARACTER.
//  o  lexer->lexkey contains 0.
//  o  lexer->offending_char contains the offending character.
//  o  lexer->offending_char_pos contains the position of the offending char.
//  o  lexer->status contains
//     - I2P_LEXER_STATUS_LITERAL_TOO_LONG if maximum length is exceeded,
//     - I2P_LEXER_STATUS_STRING_NOT_DELIMITED if EOF is reached,
//     - I2P_LEXER_STATUS_ILLEGAL_CHARACTER if illegal characters are found.
//  o  characters in the input stream are skipped until a matching closing
//     quotation mark delimiter or EOF is found.
//  o  the new lookahead character is the character following the literal.

static fmacro uchar_t get_quoted_string(i2p_lexer_s *lexer) {
    uchar_t ch, delimiter_ch;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    lexer->lexeme.length = 0;
    lexer->lexkey = HASH_INITIAL;
    
    // copy opening delimiter
    ch = readchar();
    delimiter_ch = ch;
    lexer->lexeme.string[lexer->lexeme.length] = ch;
    lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
    lexer->lexeme.length++;
    ch = nextchar();
    
    while ((ch != delimiter_ch) && (IS_NOT_CONTROL(ch)) &&
           (index < I2P_MAX_LEXEME_LENGTH) && (NOT_EOF(lexer))) {
        
        // check for escaped chars
        if (ch != BACKSLASH)
            ch = readchar();
        else // backslash escaped char
            ch = get_escaped_char(lexer);
        
        // copy the char into lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        
        // prepare for next
        ch = nextchar();
    } // end while    
    
    if (ch == delimiter_ch) {
        // copy closing delimiter
        ch = readchar();
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        
        // terminate lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
        lexer->lexkey = HASH_FINAL(lexer->lexkey);
        
        // pass back token and lexeme key
        lexer->token = TOKEN_QUOTED_STRING;
        
        // set status
		lexer->status = I2P_LEXER_STATUS_SUCCESS;
        
        // return lookahead character
        return nextchar();
    }
    else { // error
        lexer->token = TOKEN_ILLEGAL_CHAR;
        lexer->lexkey = 0;
        lexer->offending_char = ch;
        lexer->offending_char_pos = lexer->current_pos;
        
        // terminate lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
        
        // set status
        if (index >= I2P_MAX_STRING_LENGTH)
            lexer->status = I2P_LEXER_STATUS_LITERAL_TOO_LONG;
        else if (EOF_REACHED(lexer))
            lexer->status = I2P_LEXER_STATUS_STRING_NOT_DELIMITED;
        else
            lexer->status = I2P_LEXER_STATUS_ILLEGAL_CHARACTER;
        
        // skip past string
        while ((ch != delimiter_ch) && (NOT_EOF(lexer))) {
            if (ch != BACKSLASH)
                ch = readchar();
            else // backslash escaped char
                ch = get_escaped_char(lexer);
            ch = nextchar();
        } // end while
        
        // return lookahead character
        return ch;
    } // end if
} // end get_quoted_string


// ---------------------------------------------------------------------------
// private function:  get_unquoted_value(lexer)
// ---------------------------------------------------------------------------
//
// Reads an  unquoted value  from the input stream of <lexer>  and returns the
// end of line character following the value.
//
// pre-conditions:
//  o  lexer is an initialised lexer object.
//  o  the current lookahead character is the first character of the value.
//
// post-conditions:
//  o  lexer->lexeme.string contains the value's literal,
//     followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains
//     - TOKEN_NUMBER_LITERAL if the value is a number literal,
//     - TOKEN_UNQUOTED_VALUE if the value is not a number literal.
//  o  lexer->lexkey contains the key for the lexeme table.
//  o  lexer->status contains I2P_LEXER_STATUS_SUCCESS.
//  o  the new lookahead character is the character following the literal.
//
// error-conditions:
//  o  lexer->lexeme.string contains the part of the literal before the
//     offending character, followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_ILLEGAL_CHARACTER.
//  o  lexer->lexkey contains 0.
//  o  lexer->offending_char contains the offending character.
//  o  lexer->offending_char_pos contains the position of the offending char.
//  o  lexer->status contains
//     - I2P_LEXER_STATUS_LITERAL_TOO_LONG if maximum length is exceeded,
//     - I2P_LEXER_STATUS_ILLEGAL_CHARACTER if illegal characters are found.
//  o  characters in the input stream are skipped until a legal terminator
//     or EOF is found.
//  o  the new lookahead character is the character following the literal.

static fmacro uchar_t get_unquoted_value(i2p_lexer_s *lexer) {
    uchar_t ch;
	cardinal index, dot_count;
	bool seen_digits_and_dots_only;

	#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
	#endif
	
	ch = nextchar();
	
	// collect all printable characters until maximum length is reached
	// or legal terminator or illegal character is found or EOF is reached
	index = 0;
	while ((index < I2P_MAX_LEXEME_LENGTH) &&
		   (ch != SEMICOLON) && (IS_NOT_CONTROL(ch)) && (NOT_EOF(lexer))) {
		
		// treat comma as terminator if value lists are enabled
		if ((ch == COMMA) && (lexer->enable_value_lists == true))
			break;
				
		// copy the char into lexeme string
        lexer->lexeme.string[index] = ch;
		
		// consume this char and peek at the next char
		readchar();
		ch = nextchar();
		
		index++;
	} // end while
		
	// check if the maximum lexeme length has reached
	if (index >= I2P_MAX_LEXEME_LENGTH) {
		
		// assume that any following whitespace and tabs are trailing
		while ((ch == WHITESPACE) || (ch == TAB)) {
			readchar();
			ch = nextchar();
		} // end while
		
		// if the following character is not a legal terminator
		// then it would have exceeded the maximum lexeme length
		if (NOT ((ch == EOL) || (ch == SEMICOLON) || EOF(lexer) ||
			((ch == COMMA) && (lexer->enable_value_lists == true)))) {
			
			// error detected : maximum lexeme length exceeded
			// set status and error information in lexer state
			lexer->lexeme.string[index] = CSTRING_TERMINATOR;
			lexer->status = I2P_LEXER_STATUS_LITERAL_TOO_LONG;
			lexer->offending_char = ch;
			lexer->offending_char_pos = lexer->current_pos;
			lexer->token = TOKEN_ILLEGAL_CHAR;
			
			// skip to first legal terminator in input stream
			repeat {
				readchar();
				ch = nextchar();
			} until ((ch == EOL) ||
					 (ch == SEMICOLON) ||
					 (EOF_REACHED(lexer)) ||
					 ((ch == COMMA) && (lexer->enable_value_lists == true))));
			
			// bail out and return lookahead character
			return ch;
		} // end if
	} // end if
	
	// check if an illegal character has been found
	if ((ch != EOL) && (NOT_EOF(lexer)) && (IS_CONTROL(ch))) {
		
		// error detected : illegal character found
		// set status and error information in lexer state
		lexer->lexeme.string[index] = CSTRING_TERMINATOR;
		lexer->status = I2P_LEXER_STATUS_ILLEGAL_CHARACTER;
		lexer->offending_char = ch;
		lexer->offending_char_pos = lexer->current_pos;
		lexer->token = TOKEN_ILLEGAL_CHAR;
		
		// skip to first legal terminator in input stream
		repeat {
			readchar();
			ch = nextchar();
		} until ((ch == EOL) ||
				 (ch == SEMICOLON) ||
				 (EOF_REACHED(lexer)) ||
				 ((ch == COMMA) && (lexer->enable_value_lists == true))));
		
		// bail out and return lookahead character
		return ch;
	} // end if
		
	// trim the lexeme buffer of any trailing whitespace and tabs
	index--;
	while ((index >= 0) &&
		   ((lexer->lexeme.string[index] == WHITESPACE) ||
		   (lexer->lexeme.string[index] == TAB)))
		index--;
	
	// terminate the lexeme string
	lexer->lexeme.length = index + 1;
	lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
	
	// prepare to determine token
	dot_count = 0;
	seen_digits_and_dots_only = true;
	
	// prepare to calculate hash
	lexer->lexkey = HASH_INITIAL;
	
	// inspect all characters of the trimmed lexeme
	index = 0;
	while (index < lexer->lexeme.length) {
		
		ch = lexer->lexeme.string[index];
		
		// count any dot
		if (ch == DOT)
			dot_count++;
		// remember any occurence of chars other than dots and digits
		else if (IS_NOT_DIGIT(ch))
			seen_digits_and_dots_only = false;
		
		// incrementally calculate hash
		lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
		
		index++;
	} // end while
	
	// finalise hash
	lexer->lexkey = HASH_FINAL(lexer->lexkey);
	
	// determine token
	
	// the symbol is a number literal if and only if
	// o  it contains at most one single dot and digits only
	// o  its first and last characters are not dots
	if ((dot_count <= 1) && (seen_digits_and_dots_only) &&
		(lexer->lexeme.string[0] != DOT) &&
		(lexer->lexeme.string[lexer->lexeme.length - 1] != DOT)) {
		
		lexer->token = TOKEN_NUMBER_LITERAL;
	}
	// otherwise the symbol is an unquoted value
	else {
		lexer->token = TOKEN_UNQUOTED_VALUE;
	} // end if
	
	lexer->status = I2P_LEXER_STATUS_SUCCESS;
	
	// return lookahead character
	return nextchar();
} // end get_unquoted_value


// ---------------------------------------------------------------------------
// private function:  get_comment(lexer)
// ---------------------------------------------------------------------------
//
// Reads a  comment  from the input stream  of <lexer>  and returns the end of
// line character following the comment.

static fmacro uchar_t get_comment(i2p_lexer_s *lexer) {
    uchar_t ch;
	cardinal index;
    
	ch = nextchar();
	lexer->lexkey = HASH_INITIAL;
	
	index = 0;
    while ((index < I2P_MAX_LEXEME_LENGTH) &&
		   (IS_NOT_CONTROL(ch)) && (NOT_EOF(lexer))) {
		
		// copy the char into lexeme string
        lexer->lexeme.string[index] = ch;
		
		// consume this char and peek at the next char
		readchar();
		ch = nextchar();

		// incrementally calculate hash
		lexer->lexkey = HASH_NEXT_CHAR(lexer->lexkey, ch);
		
		index++;
	} // end while
	
	// terminate lexeme string
	lexer->lexeme.length = index;
	lexer->lexeme.string[index] = CSTRING_TERMINATOR;
	
	// finalise hash
	lexer->lexkey = HASH_FINAL(lexer->lexkey);
	
	lexer->token = TOKEN_COMMENT;
	lexer->status = I2P_LEXER_STATUS_SUCCESS;
	
	// skip over any excess characters
	while ((IS_NOT_CONTROL(ch)) || (NOT_EOF(lexer))) {
		readchar();
		ch = nextchar();
	} // end while
    
    return ch;
} // end get_comment


// ---------------------------------------------------------------------------
// private function:  get_escaped_char(lexer)
// ---------------------------------------------------------------------------
//
// Reads an  escaped character sequence  from the input stream of <lexer>  and
// returns the character represented by the escape sequence.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//  o  current character is *assumed* to be backslash
//
// post-conditions:
//  if the assumed backslash starts an escape sequence
//  o  the current character is the last character in the escape sequence
//  o  the lookahead character is the character following the escape sequence
//  o  line and coloumn counters are updated
//
//  if the assumed backslash does not start an escape sequence
//  o  current character, lookahead character, line and coloumn counter
//     remain unchanged
//
// return-value:
//  if the assumed backslash starts an escape sequence
//  o  the escaped character is returned
//
//  if the assumed backslash does not start an escape sequence
//  o  a backslash is returned

static fmacro uchar_t get_escaped_char(i2p_lexer_s *lexer) {
    uchar_t ch, nextch;
    bool escape_sequence_found = false;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // must NOT consume current character
    // simply assume that it is backslash
    ch = BACKSLASH;
    
    // get the lookahead character
    nextch = nextchar();
    
    switch (nextch) {
        case DOUBLE_QUOTE :
        case SINGLE_QUOTE :
            escape_sequence_found = true;
            ch = nextchar();
            break;
        case DIGIT_ZERO :
            escape_sequence_found = true;
            ch = ASCII_NUL;
            break;
        case LOWERCASE_N :
            escape_sequence_found = true;
            ch = LINEFEED;
            break;
        case LOWERCASE_R :
            escape_sequence_found = true;
            ch = CARRIAGE_RETURN;
            break;
        case LOWERCASE_T :
            escape_sequence_found = true;
            ch = TAB;
            break;
        case BACKSLASH :
            escape_sequence_found = true;
            ch = BACKSLASH;
    } // end switch
    
    // consume current character only if escape sequence was found
    if (escape_sequence_found)
        readchar();
    
    return ch;
} // end get_escaped_char


// ---------------------------------------------------------------------------
// private function:  add_lexeme_to_lextab(lexer)
// ---------------------------------------------------------------------------
//
// Adds the lexer's current lexeme to its accociated lexeme table.
//
// pre-conditions:
//  o  lexer is an initialised lexer object.
//  o  lexer->lextab is an initialised KVS table object.
//  o  lexer->lexeme.string is a properly terminated C string containing the
//     lexeme of the current symbol.
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->lexkey contains the key for lexer->lexeme.string.
//
// post-conditions:
//  o  lexer->lexeme.string has been entered into lexer->lextab.
//  o  lexer->status contains I2P_LEXER_STATUS_SUCCESS.
//
// error-conditions:
//  if memmory allocation failed:
//  o  no entry has been added to lexer->lextab.
//  o  lexer->status contains I2P_LEXER_STATUS_ALLOCATION_FAILED.

static fmacro void add_lexeme_to_lextab(i2p_lexer_s *lexer) {
    kvs_status_t status;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if ((lexer == NULL) || (lexer-lextab == NULL)) return;
#endif
    
    kvs_store_value(lexer->lextab,
                    lexer->lexkey,
                    lexer->lexeme.string,
                    lexer->lexeme.length, true, &status);
    
    if (status == KVS_STATUS_ALLOCATION_FAILED)
        lexer->status = status;
    
} // end add_lexeme_to_lextab


// ---------------------------------------------------------------------------
// private function:  skip_comment(lexer)
// ---------------------------------------------------------------------------
//
// Skips past the current comment in the input stream of <lexer>  and  returns
// the end-of-line character that terminates the comment.
//
// pre-condition:
//  o  lexer is an initialised lexer object.
//  o  the lookahead character is the semicolon that starts the comment 
//
// post-conditions:
//  o  the new lookahead character is the first control character found
//  o  the lexer's coloumn counter has been updated.

static fmacro uchar_t skip_comment(i2p_lexer_s *lexer) {
	uchar_t ch;

#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // consume the opening semicolon
    readchar();
    
	ch = nextchar();
	
    // skip all characters until control character or end-of-file is found
    while ((IS_NOT_CONTROL(ch)) && (NOT_EOF(lexer))) {
        readchar();
		ch = nextchar();
	} // end while
    
    // return the lookahead character
    return ch;
} // end skip_comment


// ---------------------------------------------------------------------------
// private function:  skip_past_end_of_line(lexer)
// ---------------------------------------------------------------------------
//
// Skips past the next end-of-line in the input stream of <lexer>  and returns
// the character following end-of-line.
//
// pre-condition:
//  o  lexer is an initialised lexer object.
//
// post-conditions:
//  o  the new lookahead character is  the character following the end-of-line
//     marker.
//  o  the lexer's line and coloumn counters have been updated.

static fmacro uchar_t skip_past_end_of_line(i2p_lexer_s *lexer) {
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // consume the lookahead character
    readchar();
    
    // skip all characters until end-of-line marker is found
    while ((NOT_EOF(lexer)) && (nextchar() != EOL))
        readchar();
    
    // skip past the end-of-line marker
    readchar();
    
    // return the lookahead character
    return nextchar();
} // end skip_past_end_of_line


// END OF FILE
