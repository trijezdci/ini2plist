/* INI to Property List File Conversion Utility
 *
 *  @file i2p_lexer.h
 *  lexer interface
 *
 *  Lexical analysis for INI files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */


#ifndef I2P_LEXER_H
#define I2P_LEXER_H


#include "KVS.h"


// --------------------------------------------------------------------------
// Opaque lexer handle type
// --------------------------------------------------------------------------
//
// WARNING: Objects of this opaque type should only be accessed through this
// public interface.  DO NOT EVER attempt to bypass the public interface.
//
// The internal data structure of this opaque type is HIDDEN  and  MAY CHANGE
// at any time WITHOUT NOTICE. Accessing the internal data structure directly
// other than through the  functions  in this public interface is  UNSAFE and
// may result in an inconsistent program state or a crash.

typedef opaque_t i2p_lexer_t;


// --------------------------------------------------------------------------
// Status codes
// --------------------------------------------------------------------------

typedef /* i2p_lexer_status_t */ enum {
    I2P_LEXER_STATUS_UNDEFINED = -1,
    
    // operation completed successfully
    I2P_LEXER_STATUS_SUCCESS = 0,
    
    // invalid pointer to lexer object passed
    I2P_LEXER_STATUS_INVALID_REFERENCE,
    
    // unable to allocate memory
    I2P_LEXER_STATUS_ALLOCATION_FAILED,
    
    // illegal character found
    I2P_LEXER_STATUS_ILLEGAL_CHARACTER,
    
    // literal exceeds maximum length
    I2P_LEXER_STATUS_LITERAL_TOO_LONG,
        
    // literal is missing delimiting quotation
    I2P_LEXER_STATUS_STRING_NOT_DELIMITED,
    
} i2p_lexer_status_t;


// --------------------------------------------------------------------------
// function:  i2p_new_lexer(infile, lextab, status)
// --------------------------------------------------------------------------
//
// Creates  and  returns  a  new  lexer object  associated  with  source file 
// <infile> and lexeme table <lextab>.  The status of the operation is passed
// back in <status> unless NULL is passed in for <status>.
//
// Returns NULL if the lexer object could not be created.

i2p_lexer_t i2p_new_lexer(FILE *infile,
               i2p_kvs_table_t lextab,
            i2p_lexer_status_t *status);


// ---------------------------------------------------------------------------
// function:  i2p_lexer_getsym(lexer, lexeme, status)
// ---------------------------------------------------------------------------
//
// Reads one symbol from the input stream of lexer <lexer>, returns its token,
// and passes a key for its lexeme back in <lexeme> unless  NULL  is passed in
// for <lexeme>.  The  status  of  the  operation  is  passed back in <status>
// unless NULL is passed in for <status>.

i2p_token_t i2p_lexer_getsym(i2p_lexer_t lexer,
                               cardinal *lexeme
                     i2p_lexer_status_t *status);


// ---------------------------------------------------------------------------
// function:  i2p_lexer_set_options(lexer, skip_comments, enable_value_lists)
// ---------------------------------------------------------------------------
//
// Changes the options of <lexer> to <options>.  The function returns  without
// any action if NULL is passed in for <lexer>.

void i2p_lexer_set_options(i2p_lexer_t lexer,
                                  bool skip_comments,
                                  bool enable_value_lists);


// ---------------------------------------------------------------------------
// function:  i2p_lexer_getpos(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Obtains the position of the last symbol read from the input stream.  Passes
// the row back in <row>  unless NULL is passed in for <row>,  and the coloumn
// back in <col>  unless  NULL  is  passed  in  for <col>.  The status  of the
// operation is passed back in <status> unless NULL is passed in for <status>.

void i2p_lexer_getpos(i2p_lexer_t lexer,
                        cardinal *row,
                        cardinal *col,
              i2p_lexer_status_t *status);


// ---------------------------------------------------------------------------
// function:  i2p_offending_char(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Returns the offending character of the last read operation  and  passes its
// position  back  in  <row>  and <col>.  If no error occurred during the last
// read operation then ASCII NUL is returned  and zero is passed pack in <row>
// and <col>.  This function should only be called  after a preceeding call to
// function i2p_lexer_getsym()  returned  an error  indicating that an illegal
// or unexcpected character was found.  The status of the operation  is passed
// back in <status> unless NULL is passed in for <status>.

char i2p_offending_char(i2p_lexer_t lexer,
                          cardinal *row,
                          cardinal *col,
                i2p_lexer_status_t *status);


// ---------------------------------------------------------------------------
// function:  i2p_dispose_lexer(lexer, status)
// ---------------------------------------------------------------------------
//
// Deallocates  lexer object <lexer>.  The function does  not  close the input
// stream  and  it  does  not  deallocate the lexeme table associated with the
// lexer object.  The  status  of  the  operation  is  passed back in <status>
// unless NULL is passed in for <status>.

void i2p_dispose_lexer(i2p_lexer_t lexer,
               i2p_lexer_status_t *status);


#endif /* I2P_LEXER_H */

// END OF FILE