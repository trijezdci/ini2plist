/* INI to Property List File Conversion Utility
 *
 *  @file i2p_tokens.h
 *  token interface
 *
 *  Lexical analysis for INI files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */

#ifndef I2P_TOKENS_H
#define I2P_TOKENS_H

// ---------------------------------------------------------------------------
// Token type
// ---------------------------------------------------------------------------

typedef enum /* i2p_token_t */ {
    TOKEN_ILLEGAL_CHAR = 0,
    TOKEN_IDENTIFIER,
    TOKEN_NUMBER_LITERAL,
    TOKEN_QUOTED_STRING,
    TOKEN_UNQUOTED_VALUE,
    TOKEN_ASSIGN_OVERWRITE,
    TOKEN_ASSIGN_ACCUMULATE,
    TOKEN_OPENING_BRACKET,
    TOKEN_CLOSING_BRACKET,
    TOKEN_COMMA,
    TOKEN_COMMENT,
    TOKEN_END_OF_LINE,
    TOKEN_END_OF_FILE,
    I2P_NUMBER_OF_TOKENS
} i2p_token_t;


// ---------------------------------------------------------------------------
// function:  i2p_token_name( token )
// ---------------------------------------------------------------------------
//
// Returns a human readable string representation of token <token>.

const char *i2p_token_name(i2p_token_t token);


#endif /* I2P_TOKENS_H */

// END OF FILE