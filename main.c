/* INI to Property List File Conversion Utility
 *
 *  @file main.c
 *  main program
 *
 *  Main driver program
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h> /* command line argument processing */

#include "pathnames.h"

// --------------------------------------------------------------------------
// Version info string
// --------------------------------------------------------------------------

#define VERSION_INFO \
    "ini2plist version 1.00\n"

// --------------------------------------------------------------------------
// Copyright notice
// --------------------------------------------------------------------------

#define COPYRIGHT_NOTICE \
    "INI file to property list converter\n" \
    "Copyright (C) 2010 Benjamin Kowarsch. All rights reserved.\n" \
    "This software is provided without warranty of any kind.\n"

// --------------------------------------------------------------------------
// Return status
// --------------------------------------------------------------------------

#define SUCCESS	0
#define FAILURE 1

// --------------------------------------------------------------------------
// filename buffers
// --------------------------------------------------------------------------

char _filename[MAX_PATHNAME_LENGTH] = "";
char _infile[MAX_PATHNAME_LENGTH] = "";
char _outfile[MAX_PATHNAME_LENGTH] = "";

// --------------------------------------------------------------------------
// function print_version()
// --------------------------------------------------------------------------

void print_version() {
    printf(VERSION_INFO);
} // end print_version

// --------------------------------------------------------------------------
// function print_copyright()
// --------------------------------------------------------------------------

void print_copyright() {
    printf(COPYRIGHT_NOTICE);
} // end print_copyright

// --------------------------------------------------------------------------
// function print_usage()
// --------------------------------------------------------------------------

void print_usage() {
    print_version();
    print_copyright();
    printf("usage: ini2plist [options] infile outfile\n");
    printf("options:\n");
    printf("   -h   display this help and exit\n");
    printf("   -c   ask for confirmation before overwriting files\n");
    printf("   -D   dump infile symbol stream (for debugging)\n");
    printf("   -V   display software version and exit\n");
    printf("   -x   exclude comments\n");
    printf("   -l   enable value lists\n");
} // end print_usage

// --------------------------------------------------------------------------
// function main()
// --------------------------------------------------------------------------

int main (int argc, const char * argv[]) {
    int c, index;
	
    bool confirm_overwrite = false;
    bool dump_symbols = false;
    bool exclude_comments = false;
    bool enable_value_lists = false;
    
    char *filename = (char *)&_filename;
    char *infile = (char *)&_infile;
    char *outfile = (char *)&_outfile;
    
    // process options
    while ((c = getopt (argc, (void *)argv, "hcDVxl")) != -1) {
        switch (c) {
            case 'h':
                print_usage();
                return SUCCESS;
            case 'c':
                confirm_overwrite = true;
                break;
            case 'D':
                dump_symbols = true;
                break;
            case 'V':
                print_version();
                return SUCCESS;
            case 'x':
                exclude_comments = true;
                break;
            case 'l' :
                enable_value_lists = true;
                break;
            default:
                // invalid option
                print_usage();
                return FAILURE;
        } // end switch
    } // end while	
    
    // determine infile
    index = optind;
    if (index >= argc) {
        printf("infile not specified, conversion aborted.");
        return FAILURE;
    }
    else {
        strncpy(filename, argv[index], strlen(argv[index]));
    } // end if
    
    // check if infile is a path
    if (isPathname(filename)) {
        // just in case it's got a leading dot-slash ...
        copyStrPathnameByExpandingLeadingDotInPath(infile, filename);
    }
    else {
        // prepend filename with user's working directory
        copyStrFromWorkingDirectory(infile);
        modifyPathnameByAppendingFilename(infile, filename);
    } // end if
    
    // check if infile exists, abort if it doesn't
    if (fileDoesNotExistAtPath(infile)) {
        if (errno == EACCES) {
            printf("access to infile denied\n");
        }
        else {
            printf("infile does not exist\n");
        } // end if
        return FAILURE;
    } // end if
    
    // determine outfile
    index++;
    if (index >= argc) {
        if (verbosity > 0) printf("outfile not specified, using default.\n");
        // copy directory name from expanded infile to outfile
        copyStrDirectoryByRemovingFilenameAndExtension(outfile, infile);
        
        // obtain filename from infile
        // replace extension with plist extension
        strncat(filename, infile, strlen(filename));
        modifyFilenameByReplacingFileExtension(filename, PLIST_FILE_EXTENSION);
        
        // append the new filename to outfile
        modifyPathnameByAppendingFilename(outfile, filename);
    }
    else {
        strncpy(filename, argv[index], strlen(argv[index]) + 1);
        // check if outfile is a path
        if (isPathname(filename)) {
            // just in case it's got a leading dot-slash ...
            copyStrPathnameByExpandingLeadingDotInPath(outfile, filename);
        }
        else {
            // prepend filename with user's working directory
            copyStrFromWorkingDirectory(outfile);
            modifyPathnameByAppendingFilename(outfile, filename);
        } // end if
    } // end if
    
    // TO DO : handle confirmation mode
    
    // TO DO : run converter on infile
    
    return SUCCESS;
} // end main

// END OF FILE
